#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Elem {
    struct Elem * next;
    char * data;
};

struct Stack {
    struct Elem * top;
};

int init(struct Stack * stack) {
    if (!stack)
        return -1;

    stack->top = NULL;
    return 0;
}

int push(struct Stack * stack, char * data) {
    if (!data || !stack)
        return -1;

    struct Elem * elem = malloc(sizeof(struct Elem));

    if (!elem)
        return -1;

    elem->data = malloc(strlen(data) + 1);

    if (!elem->data)
        return -1;

    strcpy(elem->data, data);
    elem->next = stack->top;
    stack->top = elem;
    return 0;
}


void pop(struct Stack * stack) {
    if (!stack)
        return;

    if (!stack->top)
        return;

    struct Elem * elem = stack->top;
    stack->top = stack->top->next;

    free(elem->data);
    free(elem);
}


char * top(struct Stack * stack) {
    return stack->top->data;
}


void clear(struct Stack * stack) {
    if (!stack)
        return;

    while (stack->top)
        pop(stack);
}


// функции, относящиеся к логике программы

void freeStacks(struct Stack * stacks, int count) {
    for (int i = 0; i < count; ++i)
        clear(stacks[i]);
}


int main()
{
//    struct Stack s;
//    init(&s);

//    for (int i = 0; i < 5; ++i) {
//        push(&s, "stroka");

//        if (i & 1)
//            pop(&s);
//    }

//    struct Elem * elem = s.top;
//    while (elem) {
//        printf("%s\n", elem->data);
//        elem = elem->next;
//    }

//    clear(&s);

    char buf[] = "\"1\" * 2 + 2*(  \"2\" +  \" \"*2 +  \"3\")";
    char error = "[error]";
    char * str = buf;
    int current = 0; // текущий стек
    int count = 0; // количество стеков
    Stack scopes;
    init(&scopes);

    Stack stacks[100];

    while (*str) {
        if (*str == '(') {
            push(&scopes, "(");
            init(&stacks[count]);
            ++count;
            ++current;
        }

        if (*str == ')') {
            if (top(&stacks[current])[0] == '(') {
                pop(&stacks[current]);
                --current;
            } else { // ошибка в неправильном количестве закрывающих скобок
                freeStacks(&stacks, count);
                clear(&scopes);
                printf(error);
                return 0;
            }
        }

        // выше был выбран нужный стек, в который писать текущий символ
        char * item = readItem(&str);
        if (!item && *str) {    // ошибка - элемент распознан неправильно
            freeStacks(&stacks, count);
            clear(&scopes);
            printf(error);
            return 0;
        } else {
            push(&stacks[current], item);
        }





    }

    return 0;
}
