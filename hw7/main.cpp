#include <iostream>
#include "matrix.h"

using namespace std;

template <class T, size_t SIZE>
void sqrMatrix(Matrix<T, SIZE> * arr, size_t n, size_t idx) {
    try {
        if (idx >= n) throw "Incorrect index";
        // не знаю, как отловить выход за пределы массива, т.е. когда n не соответствует размерам передаваемого массива
        // посчитать через sizeof, насколько я понимаю, не вариант из-за выравнивания
    }
    catch (const char * s) {
        cerr << s << endl;
        throw;
    }
    
    arr[idx] *= arr[idx];
}

int main() try
{
    // example for int
    Matrix <> arr_i[3];
    arr_i[0].init(2);
    arr_i[0](5, 6) = 0;
    arr_i[1] = arr_i[0] + arr_i[0];
    arr_i[2].init(1);
    arr_i[2] *= arr_i[1];
    cout << arr_i[2] << endl;
    sqrMatrix(arr_i, 3, 2);
    cout << arr_i[2] << endl;
    
    // example for double
    Matrix <double, 3> arr_d[2];
    arr_d[0].init(5.5);
    arr_d[0](1, 2) = 2.0;
    arr_d[1] += arr_d[0];
    cout << arr_d[1] << endl;
    sqrMatrix(arr_d, 2, 1);
    cout << arr_d[1] << endl;
    
    return 0;
}
catch (...) {
    cerr << "Panic!" << endl;
    return 1;
}

