#ifndef MATRIX_H
#define MATRIX_H

#include <cmath>
#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
using std::ostream;


template<typename T = int, size_t SIZE = 10>
class Matrix
{
public:
    Matrix();
    Matrix(const Matrix&);
    Matrix& operator= (const Matrix& rht);
    T& operator() (size_t i, size_t j);
    const T& operator() (size_t i, size_t j) const;
    ~Matrix() { delete [] _matrix; }
    
    Matrix operator+ (const Matrix& m) { return this->add(m); }
    Matrix operator* (const Matrix& m) { return this->mul(m); }
    Matrix operator- (const Matrix& m) { return this->sub(m); }
  
    Matrix& operator+= (const Matrix& m);    
    Matrix& operator*= (const Matrix& m);
    Matrix& operator-= (const Matrix& m);
    
    void init (const T value);
    double calcNorm();
    
private:
    T * _matrix;
    template <typename T1, size_t S> friend  ostream& operator << (ostream &, const Matrix<T1, S> &);
    Matrix add (const Matrix& m);
    Matrix mul (const Matrix& m);
    Matrix sub (const Matrix& m);
   
};



template <typename T, size_t SIZE>
Matrix<T, SIZE>::Matrix() : _matrix(nullptr)
{   
    try {
        _matrix = new T[SIZE * SIZE * sizeof(T)];
    } 
    catch (...) {
        cerr << "Error with memory allocation" << endl;
        return;
    }
    
    this->init(0);
}

template <typename T, size_t SIZE>
void Matrix<T, SIZE>::init(const T val) 
{
    for (size_t i = 0; i < SIZE * SIZE; ++i) {
        _matrix[i] = val;
    }
}

template <typename T, size_t SIZE>
T& Matrix<T, SIZE>::operator()(size_t i, size_t j)
{
    try {
        if (_matrix == nullptr) throw(_matrix);
        if (i >= SIZE || j >= SIZE) throw(std::max(i, j));
    }
    catch (T * ptr) {
        cerr << "The matrix isn't determined" << endl;
        throw;
    }
    catch (size_t n) {
        cerr << "Incorrect value " << n << endl;
        throw;
    }
    
    return *(_matrix + i * SIZE + j); 
}


template <typename T, size_t SIZE>
const T& Matrix<T, SIZE>::operator()(size_t i, size_t j) const
{
    try {
        if (_matrix == nullptr) throw(_matrix);
        if (i >= SIZE || j >= SIZE) throw(std::max(i, j));
    }
    catch (T * ptr) {
        cerr << "The matrix isn't determined" << endl;
        throw;
    }
    catch (size_t n) {
        cerr << "Incorrect value " << n << endl;
        throw;
    }
    
    return *(_matrix + i * SIZE + j); 
}


template <typename T, size_t SIZE>
ostream & operator<<(ostream & out, const Matrix<T, SIZE> & Mtr)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            out << Mtr(i, j) << ' ';
        }
        out << endl;
    }
    return out;
}

template <typename T, size_t SIZE>
Matrix<T, SIZE>& Matrix<T, SIZE>::operator= (const Matrix<T, SIZE>& rht)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            (*this)(i, j) = rht(i, j);
        }
    }
    
    return *this;
}

template <typename T, size_t SIZE>
Matrix<T, SIZE>::Matrix(const Matrix<T, SIZE>& cpy)
{
    *this = cpy;
}


template <typename T, size_t SIZE>
Matrix<T, SIZE> Matrix<T, SIZE>::add (const Matrix<T, SIZE>& m)
{
    Matrix<T, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            temp(i, j) = (*this)(i, j) + m(i, j);
        }
    }
    
    return temp;
}

template <typename T, size_t SIZE>
Matrix<T, SIZE> Matrix<T, SIZE>::sub (const Matrix<T, SIZE>& m)
{
    Matrix<T, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            temp(i, j) = (*this)(i, j) - m(i, j);
        }
    }
    
    return temp;
}

template <typename T, size_t SIZE>
Matrix<T, SIZE> Matrix<T, SIZE>::mul(const Matrix<T, SIZE>& m)
{
    Matrix<T, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            T val = 0;
            for (auto k = 0; k < SIZE; ++k) {
                val += (*this)(i, k) * m(k, j);
            }
            temp(i, j) = val;
        }
    }
    
    return temp;
}

template <typename T, size_t SIZE>
Matrix<T, SIZE>& Matrix<T, SIZE>::operator+= (const Matrix<T, SIZE>& m)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            (*this)(i, j) += m(i, j);
        }
    }
    
    return *this;
}

template <typename T, size_t SIZE>
Matrix<T, SIZE>& Matrix<T, SIZE>::operator-= (const Matrix<T, SIZE>& m)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            (*this)(i, j) -= m(i, j);
        }
    }
    
    return *this;
}

template <typename T, size_t SIZE>
Matrix<T, SIZE>& Matrix<T, SIZE>::operator*= (const Matrix<T, SIZE>& m)
{
    Matrix<T, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            T val = 0;
            for (auto k = 0; k < SIZE; ++k) {
                val += (*this)(i, k) * m(k, j);
            }
            temp(i, j) = val;
        }
    }
    
    *this = temp;
    return *this;
}


template <typename T, size_t SIZE>
double Matrix<T, SIZE>::calcNorm()
{
    T val = 0;
    for (size_t i = 0; i < SIZE; ++i) {
        for (size_t j = 0; j < SIZE; ++j) {
            val += (*this)(i, j) * (*this)(i, j);
        }
    }
    return sqrt(val);
}




// СПЕЦИАЛИЗАЦИЯ ДЛЯ T*
template<typename T, size_t SIZE>
class Matrix<T*, SIZE>
{
public:
    Matrix();
    Matrix(const Matrix&);
    Matrix& operator= (const Matrix& rht);
    T * & operator() (size_t i, size_t j);
    T * const & operator() (size_t i, size_t j) const;
    ~Matrix() { delete [] _matrix; }
    
    Matrix operator+ (const Matrix& m) { return this->add(m); }
    Matrix operator* (const Matrix& m) { return this->mul(m); }
    Matrix operator- (const Matrix& m) { return this->sub(m); }
  
    Matrix& operator+= (const Matrix& m);    
    Matrix& operator*= (const Matrix& m);
    Matrix& operator-= (const Matrix& m);
   
    void init(T * const val);
    double calcNorm();
    
private:
    T ** _matrix;
    template <typename T1, size_t S> friend  ostream& operator << (ostream &, const Matrix<T1*, S> &);
    Matrix add (const Matrix& m);
    Matrix mul (const Matrix& m);
    Matrix sub (const Matrix& m);
   
};


template <typename T, size_t SIZE>
Matrix<T*, SIZE>::Matrix() : _matrix(nullptr)
{   
    try {
        _matrix = new T*[SIZE * SIZE * sizeof(T*)];
    } 
    catch (...) {
        cerr << "Error with memory allocation" << endl;
        return;
    }
    
    this->init(0);
}

template <typename T, size_t SIZE>
void Matrix<T*, SIZE>::init(T * const val) 
{
    for (size_t i = 0; i < SIZE * SIZE; ++i) {
        _matrix[i] = val;
    }
}

template <typename T, size_t SIZE>
T* &Matrix<T*, SIZE>::operator()(size_t i, size_t j)
{
    try {
        if (_matrix == nullptr) throw(_matrix);
        if (i >= SIZE || j >= SIZE) throw(std::max(i, j));
    }
    catch (T * ptr) {
        cerr << "The matrix isn't determined" << endl;
        throw;
    }
    catch (size_t n) {
        cerr << "Incorrect value " << n << endl;
        throw;
    }
    
    return *(_matrix + i * SIZE + j); 
}


template <typename T, size_t SIZE>
T* const &Matrix<T*, SIZE>::operator()(size_t i, size_t j) const
{
    try {
        if (_matrix == nullptr) throw(_matrix);
        if (i >= SIZE || j >= SIZE) throw(std::max(i, j));
    }
    catch (T * ptr) {
        cerr << "The matrix isn't determined" << endl;
        throw;
    }
    catch (size_t n) {
        cerr << "Incorrect value " << n << endl;
        throw;
    }
    
    return *(_matrix + i * SIZE + j); 
}


template <typename T, size_t SIZE>
ostream & operator<<(ostream & out, const Matrix<T*, SIZE> & Mtr)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            out << *Mtr(i, j) << ' ';
        }
        out << endl;
    }
    return out;
}

template <typename T, size_t SIZE>
Matrix<T*, SIZE>& Matrix<T*, SIZE>::operator= (const Matrix<T*, SIZE>& rht)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            (*this)(i, j) = rht(i, j);
        }
    }
    
    return *this;
}

template <typename T, size_t SIZE>
Matrix<T*, SIZE>::Matrix(const Matrix<T*, SIZE>& cpy)
{
    *this = cpy;
}


template <typename T, size_t SIZE>
Matrix<T*, SIZE> Matrix<T*, SIZE>::add (const Matrix<T*, SIZE>& m)
{
    Matrix<T*, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            temp(i, j) = (*this)(i, j) + m(i, j);
        }
    }
    
    return temp;
}

template <typename T, size_t SIZE>
Matrix<T*, SIZE> Matrix<T*, SIZE>::sub (const Matrix<T*, SIZE>& m)
{
    Matrix<T*, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            temp(i, j) = (*this)(i, j) - m(i, j);
        }
    }
    
    return temp;
}

template <typename T, size_t SIZE>
Matrix<T*, SIZE> Matrix<T*, SIZE>::mul(const Matrix<T*, SIZE>& m)
{
    Matrix<T*, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            T * val = nullptr;
            for (auto k = 0; k < SIZE; ++k) {
                val += (*this)(i, k) * m(k, j);
            }
            temp(i, j) = val;
        }
    }
    
    return temp;
}

template <typename T, size_t SIZE>
Matrix<T*, SIZE>& Matrix<T*, SIZE>::operator+= (const Matrix<T*, SIZE>& m)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            (*this)(i, j) += m(i, j);
        }
    }
    
    return *this;
}

template <typename T, size_t SIZE>
Matrix<T*, SIZE>& Matrix<T*, SIZE>::operator-= (const Matrix<T*, SIZE>& m)
{
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            (*this)(i, j) -= m(i, j);
        }
    }
    
    return *this;
}

template <typename T, size_t SIZE>
Matrix<T*, SIZE>& Matrix<T*, SIZE>::operator*= (const Matrix<T*, SIZE>& m)
{
    Matrix<T*, SIZE> temp;
    for (auto i = 0; i < SIZE; ++i) {
        for (auto j = 0; j < SIZE; ++j) {
            T * val = nullptr;
            for (auto k = 0; k < SIZE; ++k) {
                val += (*this)(i, k) * m(k, j);
            }
            temp(i, j) = val;
        }
    }
    
    *this = temp;
    return *this;
}

template <typename T, size_t SIZE>
double Matrix<T*, SIZE>::calcNorm()
{
    T * val = nullptr;
    for (size_t i = 0; i < SIZE; ++i) {
        for (size_t j = 0; j < SIZE; ++j) {
            val += (*this)(i, j) * (*this)(i, j);
        }
    }
    return sqrt(val);
}


// СПЕЦИАЛИЗАЦИЯ ДЛЯ &
template<typename T, size_t SIZE>
class Matrix<T&, SIZE>
{
public:
    Matrix() try {
        throw "I am not sure that it's neccessary to do so I am lazy to copy a lot of code again";
    }
    catch (const char * s) {
        cout << s << endl;
        throw; 
    };
};

// СПЕЦИАЛИЗАЦИЯ ДЛЯ const
template<typename T, size_t SIZE>
class Matrix<const T, SIZE>
{
public:
    Matrix() try {
        throw "I am not sure that it's neccessary to do so I am lazy to copy a lot of code again";
    }
    catch (const char * s) {
        cout << s << endl;
        throw; 
    };
};

// СПЕЦИАЛИЗАЦИЯ ДЛЯ const &
template<typename T, size_t SIZE>
class Matrix<const T&, SIZE>
{
public:
    Matrix() try {
        throw "I am not sure that it's neccessary to do so I am lazy to copy a lot of code again";
    }
    catch (const char * s) {
        cout << s << endl;
        throw; 
    };
};

// СПЕЦИАЛИЗАЦИЯ ДЛЯ const *
template<typename T, size_t SIZE>
class Matrix<const T *, SIZE>
{
public:
    Matrix() try {
        throw "I am not sure that it's neccessary to do so I am lazy to copy a lot of code again";
    }
    catch (const char * s) {
        cout << s << endl;
        throw; 
    };
};


#endif /* MATRIX_H */

